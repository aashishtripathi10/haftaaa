var hapi = require('hapi');
var server = new hapi.Server();

server.connection({ 
	host: 'localhost',
	port: 8080
});

server.register([{ 
	register: require('inert')}, {
	register: require('blipp')
}], function(err) { 
	if(err)
		return console.log(err);

	//register the routes
	server.route(require('./routes/routes'));

	server.start(function(){ 
		console.log('Server listening');
	});
});