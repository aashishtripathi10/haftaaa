var cc = require('coupon-code');

module.exports = [ 
  
  //SIGNUP ROUTE
  //ACCEPTS: email of user (required), referral code(optional)
  //DOES: creates a new user and stores the user object in the database along with the 
  //referral information of the user
  //RETURNS: id of the user created
	{
    path: '/signup',
  	method: 'POST',
    config: {
      handler: function(req, reply) {

        var User = require('../models/user');
        var myrefCode = cc.generate({ parts : 1, partLen : 6 });
        var replystring;

        User.find({email: req.payload.email}, function(err, user) {
          if(user[0])
          {
            replystring = {"id" : user[0]._id};
            reply(replystring);
          }
          else
          {
            var newUser = new User({
              email: req.payload.email,
              myCode: myrefCode,
            });

            newUser.save(function(err) {
              if (err)
                throw err;
            });

            if(req.payload.referralcode)
            {
              User.find({myCode: req.payload.referralcode}, function(err, user) {
                if (err) 
                {
                }
                else
                {
                  newUser.referredBy = user[0]._id;
                  user[0].referredUsers.push(newUser._id);

                  newUser.save(function(err) {
                    if (err) 
                      throw err;
                    console.log('newUser referred by: ' + newUser.referredBy);
                  });
                  user[0].save(function(err) {
                    if (err)
                      throw err;
                    console.log('Referred users: ' + user[0].referredUsers);
                  });
                }
              });
            }
            console.log(newUser);
            replystring = {"id" : newUser._id};
            reply(replystring);
          }
        });
      }
    }
  },
  
  //SETUP GATEWAY
  //ACCEPTS: ID of user(required), Preferred Salary Gateway(required)
  //DOES: Finds the user using the ID and stores the supplied Salary Gateway
  //RETURNS: OK if the setup goes fine
  {
    path: '/setupgateway',
    method: 'POST',
    config: {
      handler: function(req, reply) {

        if(!req.payload.id || !req.payload.salaryGateway)
          return;

        var User = require('../models/user');
        
        User.find({_id: req.payload.id}, function(err, user) {
          if (err)
           throw err;
        
          user[0].salaryGateway = req.payload.salaryGateway;
        
          user[0].save(function(err) {
            if (err)
             throw err;
            console.log('salary Gateway for ' + user[0].email + ' is ' + user[0].salaryGateway);
          });
        });
        reply('ok');
      }
    }
  },

  //CURRENT GATEWAY
  //ACCEPTS: ID of user(required)
  //DOES: Finds the user using the ID and returns the Salary Gateway
  //RETURNS: The Salary Gateway
  {
    path: '/currentgateway',
    method: 'POST',
    config: {
      handler: function(req, reply) {

        if(!req.payload.id)
          return;

        var User = require('../models/user');
        var replystring;
  
        User.find({_id: req.payload.id}, function(err, user) {
          if (err)
           throw err;
  
          if(user[0].salaryGateway)
            replystring = { "salaryGateway" : user[0].salaryGateway }
          else
            replystring = { "salaryGateway" : "-1" }
          reply(replystring);
        });
      }
    }
  },

  //MY REFERRAL CODE
  //ACCEPTS: ID of user(required)
  //DOES: Finds the user using the ID and returns the Referral Code
  //RETURNS: The User's Referral Code
  {
    path: '/myreferrralcode',
    method: 'POST',
    config: {
      handler: function(req, reply) {

        if(!req.payload.id)
          return;

        var User = require('../models/user');
        var replystring;

        User.find({_id: req.payload.id}, function(err, user) {
          if (err)
           throw err;
          console.log(user[0]);
          replystring = { "referralcode" : user[0].myCode }
          reply(replystring);
        });
      }
    }
  },

  //UPDATE WALLET HISTORY
  //ACCEPTS: ID of user(required), current wallet balance(required)
  //DOES: Finds the user using the ID and updates the current wallet balance for that month
  //RETURNS: OK if the setup goes fine
  {
    path: '/updatewallethistory',
    method: 'POST',
    config: {
      handler: function(req, reply) {
 
        if(!req.payload.id || !req.payload.currentwalletbalance)
          return;
 
        var User = require('../models/user');
 
        User.find({_id: req.payload.id}, function(err, user) {
          if (err)
           throw err;
 
          var month = new Date().getMonth();
          var balance;
          if(month == 0)
            balance = {"January" : req.payload.currentwalletbalance};
          else if(month == 1)
            balance = {"February" : req.payload.currentwalletbalance};
          else if(month == 2)
            balance = {"March" : req.payload.currentwalletbalance};
          else if(month == 3)
            balance = {"April" : req.payload.currentwalletbalance};
          else if(month == 4)
            balance = {"May" : req.payload.currentwalletbalance};
          else if(month == 5)
            balance = {"June" : req.payload.currentwalletbalance};
          else if(month == 6)
            balance = {"July" : req.payload.currentwalletbalance};
          else if(month == 7)
            balance = {"August" : req.payload.currentwalletbalance};
          else if(month == 8)
            balance = {"September" : req.payload.currentwalletbalance};
          else if(month == 9)
            balance = {"October" : req.payload.currentwalletbalance};
          else if(month == 10)
            balance = {"November" : req.payload.currentwalletbalance};
          else if(month == 11)
            balance = {"December" : req.payload.currentwalletbalance};
          
          var balance_string = JSON.stringify(balance);
          user[0].wallethistory.push(balance_string);
 
          user[0].save(function(err) {
            if (err)
             throw err;
            console.log('Wallet Balance for ' + user[0].email + ' is ' + user[0].wallethistory);
          });
          reply('ok');
        });
      }
    }
  },

  //GET WALLET HISTORY
  //ACCEPTS: ID of user(required)
  //DOES: Finds the user using the ID and returns the wallet balance for last twelve months
  //RETURNS: The wallet balance for last twelve months
  {
    path: '/getwallethistory',
    method: 'POST',
    config: {
      handler: function(req, reply) {
        if(!req.payload.id)
          return;
  
        var User = require('../models/user');
  
        User.find({_id: req.payload.id}, function(err, user) {
          if (err) 
            throw err;

          reply(user[0].wallethistory);
        });
      }
    }
  }
];